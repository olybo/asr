
from torch import nn

class RNNASR(nn.Module):    
    def __init__(self):
        super(RNNASR, self).__init__()
          
        self.lstm = nn.LSTM(input_size=128, hidden_size=512, bidirectional=True, num_layers=2)
        self.lin = nn.Linear(1024, 28)
        
    def forward(self, input):
        x, _ = self.lstm(input)
        return self.lin(x)
