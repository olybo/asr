class TextTransform:
    """Maps characters to integers and vice versa"""
    
    letters = 'üqwertyuiopasdfgjklzxcvbnm '
    
    def __init__(self):
        self.char_to_idx = {c : i + 1 for i, c in enumerate(self.letters)}
        self.char_to_idx[''] = 0
        self.idx_to_char = [''] + list(self.letters)
    
    def is_acceptable(self, sym):
        return sym in self.letters

    def text_to_int(self, text):
        """ Use a character map and convert text to an integer sequence """
        return [self.char_to_idx[c] for c in ''.join(filter(self.is_acceptable, text.lower()))]

    def int_to_text(self, labels):
        """ Use a character map and convert integer labels to an text sequence """
        return ''.join([self.idx_to_char[int(i)] for i in labels])