from torch import distributions
import random


class GaussianNoise:
    
    def __init__(self, mean=0, sigma=0.05):
        self.mean = mean
        self.sigma = sigma
    
    def __call__(self, wav):
        noiser = distributions.Normal(self.mean, self.sigma)
        return wav + noiser.sample(wav.size())
    
    
class RandomTimeStretching:
    
    def __call__(self, mel):
        return torchaudio.transforms.TimeStretch(random.uniform(0.8, 1.3))(mel)


class RandomMelTransform:
    
    def __call__(self, wav):
        return torchaudio.transforms.MelSpectrogram(sample_rate=22050 + random.randint(-2000, 2000), n_mels=128)(wav)

    
class RandomVolume:
    def __init__(self, db_l=-30., db_h=30.):
        self.db_l = db_l
        self.db_h = db_h

    def __call__(self, wav):
        return torch.clamp(torchaudio.functional.gain(wav, random.uniform(self.db_l, self.db_h)), -1.0, 1.0)


