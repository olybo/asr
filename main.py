import torch.nn as nn
import torch
import torchaudio
from dataset import LJSpeechDataset,data_processing
import os
from torch.utils import data
from my_model import RNNASR
from torch import optim
from training import IterMeter, train, test
import torch.nn.functional as F
import numpy as np
import random
from augmentation import RandomMelTransform

seed = 0
torch.backends.cudnn.deterministic = True
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)
np.random.seed(seed)
random.seed(seed)


def main(learning_rate=5e-4, batch_size=20, epochs=10):

    hparams = {
        "n_cnn_layers": 3,
        "n_rnn_layers": 5,
        "rnn_dim": 512,
        "n_class": 29,
        "n_feats": 128,
        "stride":2,
        "dropout": 0.1,
        "learning_rate": learning_rate,
        "batch_size": batch_size,
        "epochs": epochs
    }


    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    if not os.path.isdir("./data"):
        os.makedirs("./data")
        
        
    train_audio_transforms = nn.Sequential(
        torchaudio.transforms.RandomMelTransform(sample_rate=22050, n_mels=128),
        torchaudio.transforms.FrequencyMasking(freq_mask_param=30),
        torchaudio.transforms.TimeMasking(time_mask_param=100),
    )

    valid_audio_transforms = torchaudio.transforms.MelSpectrogram(sample_rate=22050, n_mels=128)

    train_dataset = LJSpeechDataset(train_audio_transforms, train=True)
    valid_dataset = LJSpeechDataset(valid_audio_transforms, train=False)

    kwargs = {'num_workers': 8, 'pin_memory': True} if use_cuda else {'num_workers': 4}

    train_loader = data.DataLoader(dataset=train_dataset,
                                batch_size=hparams['batch_size'],
                                shuffle=True,
                                collate_fn=lambda x: data_processing(x),
                                **kwargs)
    test_loader = data.DataLoader(dataset=test_dataset,
                                batch_size=hparams['batch_size'],
                                shuffle=False,
                                collate_fn=lambda x: data_processing(x),
                                **kwargs)

    model = RNNASR().to(device)

    # print(model)
    print('Num Model Parameters', sum([param.nelement() for param in model.parameters()]))

    optimizer = optim.AdamW(model.parameters(), hparams['learning_rate'])
    criterion = nn.CTCLoss(blank=0).to(device)
    scheduler = optim.lr_scheduler.OneCycleLR(optimizer, max_lr=hparams['learning_rate'], 
                                            steps_per_epoch=int(len(train_loader)),
                                            epochs=hparams['epochs'],
                                            anneal_strategy='linear')
    
    iter_meter = IterMeter()
    for epoch in range(1, epochs + 1):
        train(model, device, train_loader, criterion, optimizer, scheduler, epoch, iter_meter)
        test(model, device, train_loader, criterion, epoch, iter_meter)
learning_rate = 5e-4
batch_size = 32
epochs = 40


main(learning_rate, batch_size, epochs)
