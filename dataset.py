import torchaudio
import numpy as np
import torchaudio
import pandas as pd
from torch.utils.data.dataset import Dataset
import torch
from torch import nn
from text_processing import TextTransform



def random_indices(size=13100, train_size=0.9): #13100
    perm = np.random.permutation(size)
    return perm[:int(train_size * size)], perm[int(train_size * size):]


class LJSpeechDataset(Dataset):
    
    audio_folder_path = "LJSpeech-1.1/wavs"
    meta_path = "LJSpeech-1.1/metadata.csv"
    train_idx, test_idx = random_indices()
    
    def __init__(self, transform_mel, transform_wav=None, train=True, max_audio_size=100000):
        if train:
            idx = self.train_idx
        else:
            idx = self.test_idx
        
        df = pd.read_csv(self.meta_path, delimiter='|', header=None)
        self.labels = df[2][idx].values
        self.audio = df[0][idx].values

        self.labels

        self.transform_mel = transform_mel
        self.transform_wav = transform_wav
        
        

    def __getitem__(self, index):
        return self.get_audio(index), self.labels[index]
    
    def get_audio(self, index):
        wav, sample_rate = torchaudio.load(self.audio_folder_path + '/' + self.audio[index] + '.wav')
        audio = wav if self.transform_wav is None else self.transform_wav(wav)
        mel = self.transform_mel(audio)
        return mel.squeeze(0).transpose(0, 1)
    
    def get_audio_size(self, filename):
        return os.path.get_audio_size(self.audio_folder_path + '/' + self.audio[index] + '.wav')
        

    def __len__(self):
        return len(self.audio)


def data_processing(data):    
    spectrograms = []
    labels = []
    input_lengths = []
    label_lengths = []
    i = 0
    for spec, utterance in data:
        spectrograms.append(spec)
        label = torch.Tensor(TextTransform().text_to_int(utterance))
        labels.append(label)
        input_lengths.append(spec.shape[0]//2)
        label_lengths.append(len(label))

    spectrograms = nn.utils.rnn.pad_sequence(spectrograms, batch_first=True).transpose(1, 2)
    labels = nn.utils.rnn.pad_sequence(labels, batch_first=True)

    return spectrograms, labels, input_lengths, label_lengths
